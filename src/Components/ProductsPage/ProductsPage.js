import { Component } from 'react'
import axios from 'axios'
import ProductCard from '../ProductCard/ProductCard'
import './ProductsPage.css'
import Error from '../Error/Error'
import Loader from '../Loader/Loader'
import { Link } from 'react-router-dom'

class ProductsPage extends Component {

    constructor(props) {
        super(props)

        this.state = {
            productsArray: [],
            status: this.APP_STATE.LOADING,

            // userEnterData:props.userEnterData
            // searchInput: ''
        }
    

    }

    APP_STATE = {
        LOADING: 'loading',
        LOADED: 'loaded',
        ERROR: 'error',
    }

    
    getProducts = () => {
        const {getApiDataArray,userEnterData}=this.props
       
        axios.get('https://fakestoreapi.com/products')
            .then((response) => {
                this.setState({
                    productsArray: [...response.data, ...userEnterData],
                    status: this.APP_STATE.LOADED
                }, () => {
                    console.log(this.state)
                    if(this.state.productsArray.length>0){
                        getApiDataArray(this.state.productsArray)
                    }
                    

                })
            })
            .catch((error) => {
                console.error(error)
                this.setState({ status: this.APP_STATE.ERROR })
            })
    }

    // onChangeSearchInput = event => { this.setState({ searchInput: event.target.value }) }


    componentDidMount = () => {
        console.log('mount')
        this.getProducts()
        



        
    }


    render() {

        const { productsArray, status } = this.state
        // const {combinedProductsArray}=this.props
        console.log(productsArray)
        // const searchResults = productsArray.filter(eachIssue => eachIssue.title.toLowerCase().includes(searchInput.toLowerCase()))


        return (
            <div className='bg-container'>
                <h1 className='dashboard-heading'>Our Products</h1>
                <div className='add-product-button-container'>
                    <Link to='addProduct'>
                        <button className='add-product-button'>Add product</button>
                    </Link>

                </div>


                <div className='products-page'>
                    {/* <div className="search-input-container">
                        < BsSearch alt="search icon" className="search-icon" />
                        <input type='search' className='search-input' placeholder='Search Products' onChange={this.onChangeSearchInput} value={searchInput} />
                    </div> */}

                    {status === this.APP_STATE.LOADING ?
                        <>
                            <Loader />
                        </>
                        :
                        undefined
                    }
                    {status === this.APP_STATE.ERROR ?
                        <>
                            < Error />
                        </>
                        :
                        undefined
                    }
                    {status === this.APP_STATE.LOADED ?
                        <>
                            {productsArray.lenth === 0 ?
                                <>
                                    No Products Available
                                </>
                                :

                                <ul className='products-container'>
                                    {productsArray.map((eachProduct) => (
                                        <ProductCard productDetails={eachProduct} key={eachProduct.id} />
                                    ))}
                                    {/* {userEnterData.map((eachProduct) => (
                                        <ProductCard productDetails={eachProduct}  />
                                    ))} */}
                                </ul>

                            }
                        </>
                        :
                        undefined
                    }



                </div>


            </div>
        )

    }
}

export default ProductsPage