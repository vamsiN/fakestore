import React, { Component } from 'react'
import './AddProductForm.css'
import validator from 'validator'
import { Link } from 'react-router-dom'


export class AddProductForm extends Component {

    constructor(props) {



        super(props)

        this.state = {
            title: '',
            titleError: '',

            category: '',
            categoryError: '',

            url: '',
            urlError: '',

            price: '',
            priceError: '',

            ratingRate: '',
            ratingRateError: '',

            ratingCount: '',
            ratingCountError: '',

            successMessage: ''
        }
    }

    handleTitleChange = (event) => {
        this.setState({
            title: event.target.value
        })
    }

    handleCategoryChange = (event) => {
        this.setState({
            category: event.target.value
        })
    }

    handleUrlChange = (event) => {
        this.setState({
            url: event.target.value
        })
    }

    handlePriceChange = (event) => {
        this.setState({
            price: event.target.value
        })
    }

    handleRatingRateChange = (event) => {
        this.setState({
            ratingRate: event.target.value
        })
    }

    handleRatingCountChange = (event) => {
        this.setState({
            ratingCount: event.target.value
        })
    }


    getProductData = (event) => {
        const { getUserAddedProductData } = this.props
        event.preventDefault()

        const {
            title,
            category,
            url,
            price,
            ratingRate,
            ratingCount,

            // titleError,
            // categoryError,
            // urlError,
            // priceError,
            // ratingRateError,
            // ratingCountError,
            
        } = this.state

        let sucess = true


        if (!(validator.isAlpha(title))) {
            this.setState({
                titleError: 'please enter valid title'
            })
            sucess = false;
        } else {
            this.setState({
                titleError: ''
            })
        }

        if (!(validator.isAlpha(category))) {
            this.setState({
                categoryError: 'please enter valid category'
            })
            sucess = false;
        } else {
            this.setState({
                categoryError: ''
            })
        }

        if (!(validator.isURL(url))) {
            this.setState({
                urlError: 'please enter valid url'
            })
            sucess = false;
        } else {
            this.setState({
                urlError: ''
            })
        }

        if (!(validator.isNumeric(price))) {
            this.setState({
                priceError: 'Price  must be grater than 0'
            })
            sucess = false;
        } else {
            this.setState({
                priceError: ''
            })
        }

        if (parseInt(ratingRate) < 0 || parseInt(ratingRate) > 5 || ratingRate === '') {
            this.setState({
                ratingRateError: 'Rating must be in the range of 0-5'
            })
            sucess = false;
        } else {
            this.setState({
                ratingRateError: ''
            })
        }

        if (parseInt(ratingCount) < 0 || ratingCount === '') {
            this.setState({
                ratingCountError: 'Rating count must be grater than 0'
            })
            sucess = false;
        } else {
            this.setState({
                ratingCountError: ''
            })
        }

        if(sucess === true){
            getUserAddedProductData(this.state)
            this.setState({
                successMessage:'your product has been submitted, you can see your product in Products Page by clicking on View all Products button'
            })
        }else{
            this.setState({
                successMessage: ''
            })
        }


        // if (!(titleError || categoryError || urlError || priceError || ratingRateError || ratingCountError) && ((title || category || url || price || ratingRate || ratingCount))) {
        //     this.setState({
        //         submitted: true,
        //         successMessage: 'your product is submitted'
        //     }, () => {
        //         console.log('getUserAddedProduct method called')
        //         getUserAddedProductData(this.state)
        //     })
        // } else {
            // this.setState({
            //     submitted: false,
            //     successMessage: ''
            // })
        // }

    }

    render() {
        return (
            <div>
                <Link to='/'>
                    <div className='products-button-container'>
                        <button type='button' className='view-all-products-button'>View all Products</button>
                    </div>
                </Link>

                <form onSubmit={this.getProductData}>
                    <div className='input-container'>
                        <label htmlFor='productTitle'>Product title: </label>
                        <br/>
                        <input className='input-feild' id='productTitle' type='text' onChange={this.handleTitleChange}></input>
                        <span className='error-message'>{this.state.titleError.length > 0 ? <div>{this.state.titleError}</div> : <div>&nbsp;</div>}</span>
                    </div>
                    <div className='input-container'>
                        <label htmlFor='productCategory'>Product Category: </label>
                        <br/>
                        <input className='input-feild' id="productCategory" type='text' onChange={this.handleCategoryChange}></input>
                        <span className='error-message'>{this.state.categoryError.length > 0 ? <div>{this.state.categoryError}</div> : <div>&nbsp;</div>}</span>
                    </div>
                    <div className='input-container'>
                        <label htmlFor='productUrl'>Product url: </label>
                        <br/>
                        <input className='input-feild' id='productUrl' type='text' onChange={this.handleUrlChange}></input>
                        <span className='error-message'>{this.state.urlError.length > 0 ? <div>{this.state.urlError}</div> : <div>&nbsp;</div>}</span>
                    </div>
                    <div className='input-container'>
                        <label htmlFor='productPrice'>Product Price: </label>
                        <br/>
                        <input className='input-feild' id='productPrice' type='text' onChange={this.handlePriceChange}></input>
                        <span className='error-message'>{this.state.priceError.length > 0 ? <div>{this.state.priceError}</div> : <div>&nbsp;</div>}</span>
                    </div>
                    <div className='input-container'>
                        <label htmlFor='productRating'>Product Rating: </label>
                        <br/>
                        <input className='input-feild' id='productRating' type='text' onChange={this.handleRatingRateChange}></input>
                        <span className='error-message'>{this.state.ratingRateError.length > 0 ? <div>{this.state.ratingRateError}</div> : <div>&nbsp;</div>}</span>
                    </div>
                    <div className='input-container'>
                        <label htmlFor='productRating'>Product Rating Count: </label>
                        <br/>
                        <input className='input-feild' id='productRatingCount' type='text' onChange={this.handleRatingCountChange}></input>
                        <span className='error-message'>{this.state.ratingCountError.length > 0 ? <div>{this.state.ratingCountError}</div> : <div>&nbsp;</div>}</span>
                    </div>

                    <div className='submit-button-container'>
                        <button type='submit' className='submit-button'>Submit</button>
                    </div>
                    <br />
                    <span className='success-message'>{this.state.successMessage.length > 0 ? <div>{this.state.successMessage}</div> : <div>&nbsp;</div>}</span>


                </form>
            </div>
        )
    }
}

export default AddProductForm