import { Link } from 'react-router-dom'
import './ProductCard.css'

const ProductCard = (props) => {

    const { productDetails } = props
    const { category, description, id, image, price, rating, title } = productDetails

    return (

        <Link to={`/products/${id}`} className='container-link'>
            <div className='card-container'>
                <div className='card-image-container'>
                    <img className='card-image' src={image} alt='productimg' />
                </div>
                <div className='card-details-container'>
                <p className='card-category'>category: {category}</p>
                    <p className='card-title'>{title}</p>
                    {/* <p className='card-description'>{description}</p> */}
                    <p className='card-price'>Price: $ {price}</p>
                    <p className='card-rating'>Rating: {rating.rate} {`(${rating.count})`}</p>
                </div>
            </div>
        </Link>



        // <div className='card-container'>
        //     <div className='card-image-container'>
        //         <img className='card-image' src={image} alt='productimg' />
        //     </div>
        //     <div className='card-details-container'>
        //         <p className='card-title'>{title}</p>
        //         <p className='card-category'>category: {category}</p>
        //         <p className='card-description'>{description}</p>
        //         <p className='card-price'>Price: $ {price}</p>
        //         <p className='card-rating'>Rating: {rating.rate} {`(${rating.count})`}</p>
        //     </div>
        // </div>


    )

}

export default ProductCard