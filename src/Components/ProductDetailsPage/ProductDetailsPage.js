import { Link, useParams } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import Loader from '../Loader/Loader'
import NotFound from '../NotFound/NotFound'

import './ProductDetailsPage.css'




const ProductDetailsPage = () => {

    const PAGE_STATE = {
        LOADING: 'loading',
        LOADED: 'loaded',
        ERROR: 'error',
    }
    const params = useParams()
    const { id } = params

    const [productDetails, setProductDetails] = useState([])
    const [isLoading, setIsLoading] = useState(PAGE_STATE.LOADING)

    useEffect(() => {
        const url = `https://fakestoreapi.com/products/${id}`

        fetch(url).then(response => response.json())
            .then(resp => {
                console.log(resp)
                setIsLoading(PAGE_STATE.LOADED)
                return setProductDetails(resp)
            })
            .catch((error) => {
                setIsLoading(PAGE_STATE.ERROR)
                console.log(error)
            })
    }, [])


    return (
        <>
            {isLoading === PAGE_STATE.LOADING ?
                <>
                    <Loader />
                </>
                :
                undefined
            }

            {isLoading === PAGE_STATE.ERROR ?
                <>
                    < NotFound />
                </>
                :
                undefined
            }

            {isLoading === PAGE_STATE.LOADED ?
                <>
                    <div className='product-detail-container'>
                        <div className='product-detail-image-container'>
                            <img className='product-detail-image' src={productDetails.image} alt='productimg' />
                        </div>
                        <div className='product-detail-text-container'>
                            <p className='product-detail-category'>category: {productDetails.category}</p>
                            <h1 className='product-detail-title'>{productDetails.title}</h1>
                            <p className='product-detail-description'>{productDetails.description}</p>
                            <p className='product-detail-price'> Price: <span className='price'>${productDetails.price}</span></p>
                            <p className='product-detail-rating'>Rating: {productDetails.rating.rate} {`(${productDetails.rating.count})`}</p>
                            <div className='buttons-container'>
                                <button className='button cart-button'>Add to cart</button>
                                <button className='button buy-button'>Buy now</button>
                                <Link to='/'>
                                    <button className='button products-button'>View all products</button>
                                </Link>


                            </div>
                        </div>

                    </div>

                </>
                :
                undefined
            }
        </>


    )










}

export default ProductDetailsPage;