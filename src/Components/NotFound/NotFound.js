import { Link } from 'react-router-dom'
import './NotFound.css'
const NotFound = () => {
    return (
        <div className='notfound-page'>
            <p>Error: Page not found</p>
            <Link to='/'>
                <button className='button products-button'>View all products</button>
            </Link>

        </div>
    )
}

export default NotFound