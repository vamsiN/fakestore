import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import ProductDetailsPage from "./Components/ProductDetailsPage/ProductDetailsPage";
import ProductsPage from './Components/ProductsPage/ProductsPage'
import NotFound from './Components/NotFound/NotFound'
import AddProductForm from "./Components/AddProductForm/AddProductForm";
import './App.css';
import { Component } from "react";

class App extends Component {
  state = {
    userEnterData: [],
    apiData: [],
  }

  getApiDataArray = (apiDataArray) => {
    this.setState(
      {
        apiData: apiDataArray,
      }
    )
  }

  getUserAddedProductData = (obj) => {

    const formattedData = {
      id: this.state.apiData.length + 1,
      title: obj.title,
      category: obj.category,
      url: obj.url,
      price: obj.price,
      rating:{
        "rate": obj.ratingRate,
        "count": obj.ratingCount
      },
      
    }

    this.setState((prevState) => (
      {
        userEnterData: [...prevState.userEnterData, formattedData],
      }
    ))

  }

  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ProductsPage userEnterData={this.state.userEnterData} getApiDataArray={this.getApiDataArray} />} />
          <Route path="/products/:id" element={<ProductDetailsPage />} />
          <Route path="/addProduct" element={< AddProductForm getUserAddedProductData={this.getUserAddedProductData} />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    )

  }




}

export default App;
